from queue import Queue
from time import sleep
import threading
import os
import sys


BUFFER_SIZE = 20
buffer = Queue(BUFFER_SIZE)
consumed_items = 0
produced_items = 0

buffer_is_available = threading.Semaphore(20)
data_is_available = threading.Semaphore(0)
mutex = threading.Lock()


def clear_screen():
    clear = ''

    if os.name == 'posix':
       clear = 'clear'
    else:
        clear = 'cls'

    os.system(clear)

def get_thread_state(name, state, number):
    str = '{}\nEstado: {}\nÍtems producidos: {}'.format(name, state, number)
    return str

def get_buffer():
    q = buffer.queue
    l = [' '] * BUFFER_SIZE

    for i in range(len(q)):
        l[i] = q[i]

    return l

def print_buffer():
    q = get_buffer()
    
    str =  '***************************************************\n'
    str += '*         *         *         *         *         *\n'
    str += '*    ' + q[0] + '    *    ' + q[1] +'    *    ' + q[2] + '    *    ' + q[3] + '    *     ' + q[4] + '   *\n'
    str += '*         *         *         *         *         *\n'
    str += '***************************************************\n'
    str += '*         *         *         *         *         *\n'
    str += '*    ' + q[5] + '    *    ' + q[6] +'    *    ' + q[7] + '    *    ' + q[8] + '    *     ' + q[9] + '   *\n'
    str += '*         *         *         *         *         *\n'
    str += '***************************************************\n'
    str += '*         *         *         *         *         *\n'
    str += '*    ' + q[5] + '    *    ' + q[6] +'    *    ' + q[7] + '    *    ' + q[8] + '    *     ' + q[9] + '   *\n'
    str += '*         *         *         *         *         *\n'
    str += '***************************************************\n'
    str += '*         *         *         *         *         *\n'
    str += '*    ' + q[5] + '    *    ' + q[6] +'    *    ' + q[7] + '    *    ' + q[8] + '    *     ' + q[9] + '   *\n'
    str += '*         *         *         *         *         *\n'
    str += '***************************************************\n'
    
    print(str)

def producer():
    global buffer, produced_items

    while True:
        buffer_is_available.acquire()
        mutex.acquire()

        buffer.put('*')
        produced_items += 1

        print(get_thread_state('Productor', 'En ejecución', produced_items) + '\n\n')
        print(get_thread_state('Consumidor', 'En espera', consumed_items) + '\n\n')
        print('En ejecución: Productor\n\n')
        print_buffer()
        
        sleep(2)
        clear_screen()
        
        mutex.release()
        data_is_available.release()

def consumer():
    global buffer, consumed_items

    while True:
        data_is_available.acquire()
        mutex.acquire()

        buffer.get()
        consumed_items += 1

        print(get_thread_state('Productor', 'En espera', produced_items) + '\n\n')
        print(get_thread_state('Productor', 'En ejecución', consumed_items) + '\n\n')
        print('En ejecución: Consumidor\n\n')
        print_buffer()

        sleep(2)
        clear_screen()

        mutex.release()
        buffer_is_available.release()
        

producer_thread = threading.Thread(target=producer)
consumer_thread = threading.Thread(target=consumer)

producer_thread.start()
consumer_thread.start()